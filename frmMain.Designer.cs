﻿namespace OctopusCityBluesLauncher
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ddlResolution = new System.Windows.Forms.ComboBox();
            this.chkFullscreen = new System.Windows.Forms.CheckBox();
            this.btnBegin = new System.Windows.Forms.Button();
            this.chkFPS = new System.Windows.Forms.CheckBox();
            this.ddlScaleMode = new System.Windows.Forms.ComboBox();
            this.lblResolution = new System.Windows.Forms.Label();
            this.lblScale = new System.Windows.Forms.Label();
            this.pbxImage = new OctopusCityBluesLauncher.NearestNeighborPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // ddlResolution
            // 
            this.ddlResolution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlResolution.FormattingEnabled = true;
            this.ddlResolution.Location = new System.Drawing.Point(12, 25);
            this.ddlResolution.Name = "ddlResolution";
            this.ddlResolution.Size = new System.Drawing.Size(156, 21);
            this.ddlResolution.TabIndex = 0;
            // 
            // chkFullscreen
            // 
            this.chkFullscreen.AutoSize = true;
            this.chkFullscreen.Location = new System.Drawing.Point(12, 92);
            this.chkFullscreen.Name = "chkFullscreen";
            this.chkFullscreen.Size = new System.Drawing.Size(74, 17);
            this.chkFullscreen.TabIndex = 1;
            this.chkFullscreen.Text = "Fullscreen";
            this.chkFullscreen.UseVisualStyleBackColor = true;
            // 
            // btnBegin
            // 
            this.btnBegin.Location = new System.Drawing.Point(12, 122);
            this.btnBegin.Name = "btnBegin";
            this.btnBegin.Size = new System.Drawing.Size(156, 26);
            this.btnBegin.TabIndex = 2;
            this.btnBegin.Text = "Indulge in Octoblood";
            this.btnBegin.UseVisualStyleBackColor = true;
            this.btnBegin.Click += new System.EventHandler(this.btnBegin_Click);
            // 
            // chkFPS
            // 
            this.chkFPS.AutoSize = true;
            this.chkFPS.Location = new System.Drawing.Point(92, 92);
            this.chkFPS.Name = "chkFPS";
            this.chkFPS.Size = new System.Drawing.Size(76, 17);
            this.chkFPS.TabIndex = 3;
            this.chkFPS.Text = "Show FPS";
            this.chkFPS.UseVisualStyleBackColor = true;
            // 
            // ddlScaleMode
            // 
            this.ddlScaleMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlScaleMode.FormattingEnabled = true;
            this.ddlScaleMode.Items.AddRange(new object[] {
            "window",
            "aspect",
            "stretch"});
            this.ddlScaleMode.Location = new System.Drawing.Point(12, 65);
            this.ddlScaleMode.Name = "ddlScaleMode";
            this.ddlScaleMode.Size = new System.Drawing.Size(156, 21);
            this.ddlScaleMode.TabIndex = 4;
            // 
            // lblResolution
            // 
            this.lblResolution.AutoSize = true;
            this.lblResolution.Location = new System.Drawing.Point(12, 9);
            this.lblResolution.Name = "lblResolution";
            this.lblResolution.Size = new System.Drawing.Size(94, 13);
            this.lblResolution.TabIndex = 5;
            this.lblResolution.Text = "Screen Resolution";
            // 
            // lblScale
            // 
            this.lblScale.AutoSize = true;
            this.lblScale.Location = new System.Drawing.Point(12, 49);
            this.lblScale.Name = "lblScale";
            this.lblScale.Size = new System.Drawing.Size(64, 13);
            this.lblScale.TabIndex = 6;
            this.lblScale.Text = "Scale Mode";
            // 
            // pbxImage
            // 
            this.pbxImage.Image = global::OctopusCityBluesLauncher.Properties.Resources.stress;
            this.pbxImage.Location = new System.Drawing.Point(174, -10);
            this.pbxImage.Name = "pbxImage";
            this.pbxImage.Size = new System.Drawing.Size(96, 144);
            this.pbxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxImage.TabIndex = 7;
            this.pbxImage.TabStop = false;
            // 
            // frmMain
            // 
            this.AcceptButton = this.btnBegin;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(274, 163);
            this.Controls.Add(this.pbxImage);
            this.Controls.Add(this.lblScale);
            this.Controls.Add(this.lblResolution);
            this.Controls.Add(this.ddlScaleMode);
            this.Controls.Add(this.chkFPS);
            this.Controls.Add(this.btnBegin);
            this.Controls.Add(this.chkFullscreen);
            this.Controls.Add(this.ddlResolution);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.ShowIcon = false;
            this.Text = "Octopus City Blues Launcher";
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ddlResolution;
        private System.Windows.Forms.CheckBox chkFullscreen;
        private System.Windows.Forms.Button btnBegin;
        private System.Windows.Forms.CheckBox chkFPS;
        private System.Windows.Forms.ComboBox ddlScaleMode;
        private System.Windows.Forms.Label lblResolution;
        private System.Windows.Forms.Label lblScale;
        private NearestNeighborPictureBox pbxImage;
    }
}

