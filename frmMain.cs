﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Nini;
using Nini.Config;
using Nini.Ini;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace OctopusCityBluesLauncher
{
    public partial class frmMain : Form
    {
        Dictionary<Resolution, bool> resolutions = new Dictionary<Resolution, bool>();
        IniConfigSource src = null;
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            try
            {
                Resolution? res = null;
                src = new IniConfigSource(
                    new IniDocument("config.ini", IniFileType.SambaStyle));
                var game = src.Configs["game"];
                if (game.Contains("screen-width") && game.Contains("screen-height"))
                {
                    res = new Resolution(game.GetInt("screen-width"),
                        game.GetInt("screen-height"));
                }
                populateScreenResolutions(res);
                ddlScaleMode.SelectedItem = game.GetString("scale-mode", "window");
                chkFullscreen.Checked = game.GetBoolean("fullscreen", false);
                chkFPS.Checked = src.Configs["debug"].GetBoolean("show-fps", false);
            }
            catch (Exception)
            {
                MessageBox.Show("Couldn't find config.ini file.");
                Close();
            }
        }
        private void populateScreenResolutions(Resolution? iniRes)
        {
            DEVMODE mode = new DEVMODE();
            int i = 0;
            while (EnumDisplaySettings(null, i, ref mode))
            {
                var res = new Resolution(mode.dmPelsWidth, mode.dmPelsHeight);
                bool acceptableSize = res.Width >= 320 && res.Height >= 240;
                if (acceptableSize && !resolutions.ContainsKey(res))
                {
                    resolutions[res] = true;
                    ddlResolution.Items.Add(res);
                    if (iniRes.HasValue && res.Equals(iniRes.Value))
                        ddlResolution.SelectedIndex = ddlResolution.Items.Count - 1;
                }
                i++;
            }
        }
        private void btnBegin_Click(object sender, EventArgs e)
        {
            var game = src.Configs["game"];
            var res = (Resolution)ddlResolution.SelectedItem;
            game.Set("screen-width", res.Width);
            game.Set("screen-height", res.Height);
            game.Set("scale-mode", ddlScaleMode.SelectedItem);
            game.Set("fullscreen", chkFullscreen.Checked.ToString().ToLower());
            src.Configs["debug"].Set("show-fps", chkFPS.Checked.ToString().ToLower());
            src.Save("config.ini");
            var currentDir = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            var filename = Path.Combine(currentDir, Path.Combine("bin", "octopus_engine.exe"));
            if (File.Exists(filename))
            {
                var startInfo = new ProcessStartInfo(filename);
                startInfo.WorkingDirectory = currentDir;
                Process proc = Process.Start(startInfo);
            }
            else
            {
                MessageBox.Show("File " + filename + " does not exist!");
            }
            Close();
        }
        public struct Resolution
        {
            public int Width { get; set; }
            public int Height { get; set; }
            public Resolution(int w, int h) : this()
            {
                Width = w;
                Height = h;
            }
            public override string ToString()
            {
                return (String.Format("{0} x {1}", Width, Height));
            }
            public override bool Equals(object obj)
            {
                if (!(obj is Resolution))
                    return false;
                Resolution res = (Resolution)obj;
                return res.Width == Width && res.Height == Height;
            }
            public override int GetHashCode()
            {
                return ToString().GetHashCode();
            }
        }
        [DllImport("user32.dll")]
        public static extern bool EnumDisplaySettings(
                string deviceName, int modeNum, ref DEVMODE devMode);
        const int ENUM_CURRENT_SETTINGS = -1;
        const int ENUM_REGISTRY_SETTINGS = -2;
                [StructLayout(LayoutKind.Sequential)]
        public struct DEVMODE
        {

            private const int CCHDEVICENAME = 0x20;
            private const int CCHFORMNAME = 0x20;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 0x20)]
            public string dmDeviceName;
            public short dmSpecVersion;
            public short dmDriverVersion;
            public short dmSize;
            public short dmDriverExtra;
            public int dmFields;
            public int dmPositionX;
            public int dmPositionY;
            public ScreenOrientation dmDisplayOrientation;
            public int dmDisplayFixedOutput;
            public short dmColor;
            public short dmDuplex;
            public short dmYResolution;
            public short dmTTOption;
            public short dmCollate;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 0x20)]
            public string dmFormName;
            public short dmLogPixels;
            public int dmBitsPerPel;
            public int dmPelsWidth;
            public int dmPelsHeight;
            public int dmDisplayFlags;
            public int dmDisplayFrequency;
            public int dmICMMethod;
            public int dmICMIntent;
            public int dmMediaType;
            public int dmDitherType;
            public int dmReserved1;
            public int dmReserved2;
            public int dmPanningWidth;
            public int dmPanningHeight;

        }
    }
}